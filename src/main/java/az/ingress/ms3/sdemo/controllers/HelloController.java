package az.ingress.ms3.sdemo.controllers;

import az.ingress.ms3.sdemo.services.HelloService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class HelloController {

    private final HelloService helloService;

    @GetMapping("/public") //world access, no authentication or authorization
    public String sayHelloPublic() {
//        log.trace("public api invoked bla bla bla");
//        log.info("public api invoked");
//        int a=0;
//        try {
//            int c=1/a;
//        }catch (Exception e){
//           log.error("Operation failed", e);
//           return "Failed to greet!";
//        }
        return "Hello World Public";
    }

    @GetMapping("/hello") //only authentication no authorization
    public String sayHello() {
        return helloService.sayHello();
    }

    @GetMapping("/user") //authentication and only users are authorized to access
    public String apiForUser() {
        return "Hello User";
    }

    @GetMapping("/admin") //authentication and only admins are authorized to access
    public String apiForAdmin() {
        return "Hello Admin";
    }

}
