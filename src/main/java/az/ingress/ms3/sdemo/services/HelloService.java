package az.ingress.ms3.sdemo.services;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

@Service
public class HelloService {

    @Secured("ROLE_ADMIN")
    public String sayHello() {
        return "Hello World";
    }
}
